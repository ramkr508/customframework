import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.nio.channels.FileChannel;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

public class MyUtility {
	
	private static final String pomPath = "C:/Users/Joy/CustomFramework/GitFolder/springbasic/", LOG = "log";
	
	public static void editProp(File file, String appName) throws IOException {	
		
		
		FileWriter fw = null; Reader fr = null; BufferedReader br = null;
		
		try{
			
			File tempFile = new File(MyStartPoint.rootPath+appName+"/src/main/resources/temp.properties");
			
			 fw = new FileWriter(tempFile);
			 fr = new FileReader(file);
			 br = new BufferedReader(fr);

			    while(br.ready()) {
			    	
			    	String str = br.readLine().replace("\\", "");
			    	
			        fw.write(str + "\n");
			    }	
			    
			    
			    
			    fw.close();
			    fr.close();
			    br.close();
			    
			    fw = new FileWriter(file);
			    fr = new FileReader(tempFile);
			    br = new BufferedReader(fr);
			    
                while(br.ready()) {
			    	
			    	String str = br.readLine();
			    	
			        fw.write(str + "\n");
			    }
			   
			
		}catch(Exception e) {	
			e.printStackTrace();
			
		}finally {
			 fw.close();
			    fr.close();
			    br.close();
		}
		
	}//end of edit method
	
	
	public static void copyFileUsingFileChannels(File source, File dest)
	        throws IOException {
	    FileChannel inputChannel = null;
	    FileChannel outputChannel = null;
	    try {
	        inputChannel = new FileInputStream(source).getChannel();
	        outputChannel = new FileOutputStream(dest).getChannel();
	        outputChannel.transferFrom(inputChannel, 0, inputChannel.size());

	    } finally {
	        inputChannel.close();
	        outputChannel.close();
	    }
	}//end of copy method
	
	
	
	public static void addLogDependency() throws ParserConfigurationException, IOException, SAXException, TransformerFactoryConfigurationError, TransformerException {

		createDependency("org.springframework.boot", "spring-boot-starter-log4j2", LOG); 
		
	
	}
	
    public static void addSwaggerDependency() throws ParserConfigurationException, IOException, SAXException, TransformerFactoryConfigurationError, TransformerException {
    	
    	createDependency("io.springfox", "springfox-swagger2", ""); 
		
	}
	
	
	
	public static void createDependency(String groupId, String artifactId, String mode) throws ParserConfigurationException, IOException, SAXException, TransformerFactoryConfigurationError, TransformerException {
		
		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance(); 
		domFactory.setIgnoringComments(true);
		DocumentBuilder builder = domFactory.newDocumentBuilder(); 
		
	    String xmlString = "";
		
	    File file = new File(pomPath+"pom.xml");
	    
	    FileReader in = new FileReader(file);
	    
	    BufferedReader br = new BufferedReader(in);
	    
	    while(br.ready()) {
	    	
	    	xmlString += br.readLine();
	    }
	    
	    br.close();
		
		StringBuilder xmlStringBuilder = new StringBuilder();
		xmlStringBuilder.append(xmlString);
		ByteArrayInputStream input = new ByteArrayInputStream(
		   xmlStringBuilder.toString().getBytes("UTF-8"));
		Document doc = builder.parse(input);
		
		doc.getDocumentElement().normalize();
		
		System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
		
		 NodeList dlist =  doc.getElementsByTagName("dependencies");
		
		
		  NodeList nList = doc.getElementsByTagName("dependency");
		  
		  
		  if(LOG.contentEquals(mode)) {
	        
	         for (int temp = 0; temp < nList.getLength(); temp++) {
	            Node nNode = nList.item(temp);
	            
	            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	               Element eElement = (Element) nNode;
	             
	               NodeList childNodes =  eElement.getChildNodes();
	               
	               for(int j=0; j< childNodes.getLength(); j++){
	            	   
	            	   Node ch = childNodes.item(j);
	  
	            	   
	            	   if(ch.getTextContent().contentEquals("spring-boot-starter-web")){
	                	   
	                	   
	                	Text grpId = doc.createTextNode(groupId); 
	               		Element ele = doc.createElement("groupId"); 
	               		ele.appendChild(grpId); 
	               		
	               		Text artId = doc.createTextNode("spring-boot-starter-logging"); 
	               		Element ele2 = doc.createElement("artifactId"); 
	               		ele2.appendChild(artId);
	               		
	               		
	               		Node exclusion = doc.createElement("exclusion"); 
	               		
	               		exclusion.appendChild(ele);  exclusion.appendChild(ele2);
	               		
	               		Node exclusions = doc.createElement("exclusions"); 
	               		
	               		exclusions.appendChild(exclusion);
	               		
	               		nNode.appendChild(exclusions);
	                	   
	                	   
	                   }
	            	   
	               }
	               
	               
	            }
	         }
	         
	}
		
	         
	   

		Text a1 = doc.createTextNode(groupId); 
		Element p1 = doc.createElement("groupId"); 
		p1.appendChild(a1); 
		
		Text a2 = doc.createTextNode(artifactId); 
		Element p2 = doc.createElement("artifactId"); 
		p2.appendChild(a2);
		
		
		
		Node p3 = doc.createElement("dependency"); 
		
		p3.appendChild(p1);  p3.appendChild(p2);
		
		dlist.item(0).appendChild(p3); 
		
		
		Source source = new DOMSource(doc);
        File xmlFile = new File(pomPath+"pom.xml");
        StreamResult result = new StreamResult(new OutputStreamWriter(
                              new FileOutputStream(xmlFile), "ISO-8859-1"));
        Transformer xformer = TransformerFactory.newInstance().newTransformer();
        xformer.transform(source, result);
		
	}
	
	
	

}
